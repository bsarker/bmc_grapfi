GrAPFI is a Graph based protein function annotation tool.
 GrAPFI uses a large network of proteins based on their domain composition. It performs a neighborhood based label propagation to discover appropriate EC Annotations for query protein. 
Please follow the below mentioned steps to Run GrAPFI. 

Prerequisites:
 
1. Get the latest version of InterProScan from https://www.ebi.ac.uk/interpro/download.html and uncompressed it in a suitable location in your machine.
Follow the instructions mentioned here for details of How to Run:
https://github.com/ebi-pf-team/interproscan/wiki/HowToRun

1. Install Python version 2.7 or upper. We recommend to have it through https://www.anaconda.com/distribution/ Version 2.7


Once you are done with installing prerequisites, you are ready to run GrAPFI.
1. Clone it from: https://gitlab.inria.fr/bsarker/BMC_GrAPFI.git or
Download the zip from https://gitlab.inria.fr/bsarker/BMC_GrAPFI and uncompress it in your machine. Move to src folder.

2. Prepare a tsv file with protein id and domains. If you have sequences in fasta format, run InterProScan to get the InterPro domains.

4. Run python ECInference.py [-i|--infile] <domainfile> [-o|--outfile] <outputfile> [-ls |--min-sim] 0.30 [-hs|--max-sim] 1.0 [-k|--top-k]
 
    a. [-i|--infile]: Tab seperated domain file

    b. [-o|--outfile]: Tab seperated EC annotations. REQUIRED.
    
    c. [-ls |--min-sim]: lower similarity threshold from 0 to 1. Default 0.3
    
    d. [-hs|--max-sim]: Higher similarity threshold. Default 1.0
    
    e. [-k|--top-k]: Top K number of prediction will be mentioned. Default 1
    


5. The description of the content:
In the folder BMC_GrAPFI, there are following python sources:
1. ECInference.py    that computes the EC prediction for a set of proteins.
2. ECInference_cv.py that perform the 10-fold cross validation over "NEW Dataset".
3. ECEvaluation.py  that computes the accuracy given truth and prediction file.
4. analysis.ipynb   perform the data characterization 


The folder "data" contains the data that have been used for experimentation. 
1. training graph
      1. sprot
      2. NEW
2. experiments
      1. The proteomes folder contains proteomes data
      2. The comparison folder contains experimental data regarding performance comparison.
      3. The NEW folder contains experimental data regarding NEW benchmark


If you are using GrAPFI or part of it, please cite:

@article{grapfi,
  TITLE = {{Exploiting Complex Protein Domain Networks for Protein Function Annotation}},
  
  AUTHOR = {Sarker, Bishnu and Ritchie, David W and Aridhi, Sabeur},
  
  journal = {7th International Conference on Complex Networks and Their Applications, Cambridge, UK},
  
  ADDRESS = {Cambridge, United Kingdom},
  
  YEAR = {2018},
  
  MONTH = Dec,
  
  KEYWORDS = {bioinformatics ; GrAPFI ; complex protein domain networks ; protein function annotation ; label propagation},
  
  pages="598--610",
  

}

Or

Sarker, Bishnu, David Ritchie, and Sabeur Aridhi. "Exploiting Complex Protein Domain Networks for Protein Function Annotation." Complex Networks 2018-7th International Conference on Complex Networks and Their Applications. 2018.




