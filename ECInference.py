from pickle import dump, load
from operator import itemgetter
from matplotlib import  pyplot as plt
import pandas as pd
from  scipy.spatial.distance import euclidean
import argparse



#========================================================
# Read the trainng graph saved in two  files
# domains_per_protein.p contains  domains per each proteins
# proteins_per_domain contains proteins  per each domains
#Loading the kidera vectors for the Training proteins
#========================================================
domains_per_protein=load(open("data/domains_per_protein.p",'rb'))
print ("Training Proteins Loaded-----\n")
proteins_per_domain=load(open("data/proteins_per_domain.p",'rb'))
print ("Training Domains are loaded\n")
#kidera_per_protein=load(open("graph data/kidera_per_protein.p"))
#=======================================================
# Read the ground truth EC labels associated with each
# protein in the pre-build graph.
#======================================================
training_EC=load(open("data/training_EC.p",'rb'))
print (" Training Labels are Loaded\n")




#===========================Similarity Functions==================
def dot(A,B): 
    return (sum(a*b for a,b in zip(A,B)))
def cosine_similarity(a,b):
    return dot(a,b) / ( (dot(a,a) **.5) * (dot(b,b) ** .5) )
def eu_distance(A,B):
    return euclidean(A,B)
def link_strength(D1, D2):
    D1=set(D1)
    D2=set(D2)
    match=len(set.intersection( D1, D2))
    union=len(set.union(D1,D2))
    return float(match)/float(union)
def containment_link_strength(D1,D2):
    D1=set(D1)
    D2=set(D2)
    match=len(set.intersection( D1, D2))
    ld1=len(D1)
    return float(match)/float(ld1)


#=======================================================
# Label Propagation
#============================================================

#========================================================
# Given a set fo domains, the following function
# finds the  neighbors in the graph who shares the dommain
#========================================================
def find_neighbors(domains ):

    neigbors=set()
    for dom in domains:

        if dom in proteins_per_domain:

            #P= [x[0] for x in proteins_per_domain[dom]]
            P=proteins_per_domain[dom]

            neigbors=set.union(neigbors,set(P))
    #print neigbors
    return neigbors
#============================================================================
# Core annotation task is accomplished by this function
#============================================================================
def annotate(node,domains, lth=0.1, hth=1.0):
    #miss_count=0
    
    #domains = Benchmark_Domains[node]#domains_per_protein[node]
    #print domains
    N=find_neighbors(domains)

    #print N
    if len(N)==0:
        print ("No Neigbors found\n")
    #print node, N
    #print 'Neighbors found', len(N)
    label = {}
    for j in N:

        if j==node:
            continue
        if j not in training_EC:
            continue
        # Retrieve the corresponding EC of the 
        ec= training_EC[j]
        if len(ec)==0:
            #print 'No EC'
            continue

        if j in domains_per_protein:
            D2=domains_per_protein[j]
        else:
            continue

        link_weight =link_strength(domains, D2)  #Jaccard Similarity
        #link_weight=containment_link_strength(domains,D2) #Containtment Similarity
        #Filter the neighbor based on their respective link weight
        if link_weight>=hth:
         #   print domains, D2
            continue
        if link_weight <lth:
            continue
        
        for e in ec:
            if e in label:
                label[e]=label[e]+link_weight
            else:
                label.update({e:link_weight})
    if len(label)>0:
        label=rankLabels(label)

    else:
        return []

    return label

#============================================================================
# To rank a dictionary by it's value and return a list of keys
#============================================================================
def normalize_weight(T):
    W=sum(T.values())*1.0
    for t in T.keys():
        T[t]=round(T[t]/W,3)
    return T 

def rankLabels(G):
    #return list(set(sorted(G, key=G.get, reverse=False)))
    G=normalize_weight(G)
    if G=={}:
        return []
    A=sorted(G.items(), key=itemgetter(1), reverse=True)
    Max = A[0][1]
    Min = Max * .50
    A = [x for x in A if x[1] >= Min]
    return A

#===============================================
# function Annotations
#===============================================

def function_annotations(nodes, domains, outfile="out.pred", lth=0.30,hth=1.0, top_k=1):
    
    W=open(outfile,'w')
    for protein in nodes:
        dom=domains[protein]
        labels = annotate(protein,dom, lth=lth, hth=hth)
        predicted_labels = labels[:top_k]
        if len(predicted_labels)>0:
            for ec in predicted_labels:
                W.write(protein+'\t'+ec[0]+'\t'+str(ec[1])+'\n')
                #print 'Prediction:',protein, ec[0], ec[1], '\n' 
        else:
            print (protein, " No Prediction","\n")
    W.close()

def read_input_file(domainfile):
    doms={}
    with open(domainfile,'r') as D:
        for line in D:
            t=line.strip().split('\t')
            doms.update({t[0]:t[1:]})
    return doms

#def main():
#    
#    #print Nodes
#    RootPath="/Volumes/Research/PhD/Projects/GrAPFI3/GrAPFI/Evaluation/UP000000625/"
#    prefix="UP000000625"
#    domain_file=RootPath+prefix+".domains"
#    
#    #=========================================================================
#    dom2node=read_input_file(domain_file)
#    Nodes=dom2node.keys()
#    for x in range(5,51):
#        lth=x/100.0
#        hth=2.0
#        pred_file=RootPath+prefix+"_"+str(lth)+"_"+str(hth) + ".pred"
#        function_annotations(Nodes, dom2node, outfile=pred_file, lth=lth, hth=hth, top_k=1)
#        print ("======= done:", lth, hth,"=================\n")
        
"""
def graph_stats():
    P=len(domains_per_protein)
    D=len(proteins_per_domain)

    avg_domains=0.0
    for p in domains_per_protein:
        avg_domains=avg_domains+len(domains_per_protein[p])
    avg_domains=avg_domains/P 
    EC1={}
    EC2={}
    EC3={}
    EC4={}
    EC=set()
    for p in training_EC:
        ecs=training_EC[p]
        for ec in ecs:
            EC.add(ec)

            l=ec.split('.')
            if not l[0]=='-':
                #EC1.add(l[0])
                if l[0] in EC1:
                    EC1[l[0]].append(p)
                else:
                    EC1.update({l[0]:[p]})
            if not l[1]=='-':
                if l[1] in EC2:
                    EC2[l[1]].append(p)
                else:
                    EC2.update({l[1]:[p]})
            if not l[2]=='-':
                if l[2] in EC3:
                    EC3[l[2]].append(p)
                else:
                    EC3.update({l[2]:[p]})
            if not l[3]=='-':
                if l[3] in EC4:
                    EC4[l[3]].append(p)
                else:
                    EC4.update({l[3]:[p]})

    
    print "Total Nodes:",P
    print "Total InterPro Signatures:",D 
    print "Total ECs", len(EC)
    print "Total EC1", len(EC1)
    print "Total EC2", len(EC2)
    print "Total EC3", len(EC3)
    print "Total EC4", len(EC4)
    for ec in EC1:
        print "Total EC for ", ec, "-->", len(EC1[ec])




    print 
"""
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--infile', required=True)
    parser.add_argument('-o', '--outfile',default="output.txt")
    parser.add_argument('-ls', '--min-sim', default=0.3)
    parser.add_argument('-hs', '--max-sim', default=1.0)
    parser.add_argument('-k', '--top-k', default=1)

    args = parser.parse_args()
    print (args.infile, args.outfile, args.min_sim, args.max_sim)

    doms=read_input_file(args.infile)

    function_annotations(doms.keys(), doms, outfile=args.outfile, lth=float(args.min_sim),hth=float(args.max_sim),top_k=int(args.top_k))
